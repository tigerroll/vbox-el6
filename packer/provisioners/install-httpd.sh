#!/usr/bin/env bash

# httpd server install.
yum -y -R 2 install httpd*

# Apache core httpd.conf configure.
HTTPD_CONFIG="/etc/httpd/conf/httpd.conf"
sed -i -e "/\#ServerName\ www\.example\.com\:80/a ServerName localhost:80" ${HTTPD_CONFIG}

# mod_proxy_ajp.so configure.
AJP_CONFIG="/etc/httpd/conf.d/proxy_ajp.conf"

cat << EOM  >> ${AJP_CONFIG}
# Think-C CI Environment Configuration.
# hello bono-bono application.
<Location /hello>
	ProxyPass ajp://127.0.0.1:8009/hello
</Location>

# jenkins CI tool.
<Location /jenkins>
	ProxyPass ajp://127.0.0.1:8109/jenkins
</Location>
EOM

# auto start on.
chkconfig httpd on
