#!/usr/bin/env bash

# Nagios install.
yum -y -R 1 install gd gd-devel net-snmp
yum --enablerepo=epel -y -R 1 install nagios \
nagios-plugins nagios-plugins-all

# auto start on.
chkconfig nagios on
