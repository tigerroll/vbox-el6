#!/usr/bin/env bash

# Java install
yum -y install \
java-1.7.0-openjdk \
java-1.7.0-openjdk-demo \
java-1.7.0-openjdk-devel \
java-1.7.0-openjdk-headless \
java-1.7.0-openjdk-src
which java
