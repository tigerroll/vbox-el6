#!/usr/bin/env bash

# Register yum repository for Jenkins.
wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins-ci.org/redhat/jenkins.repo
rpm --import http://pkg.jenkins-ci.org/redhat/jenkins-ci.org.key

# installation of jenkins
yum -y -R 2 install jenkins

# Alternatives for Java version. (openjdk-1.7.0)
alternatives --set java /usr/lib/jvm/jre-1.7.0-openjdk.x86_64/bin/java

# Configure
typeset JENKINS_CONFIG="/etc/sysconfig/jenkins"
sed -i 's/JENKINS_ARGS\=\"\"/JENKINS_ARGS\=\"\-\-prefix\=\/jenkins\"/g' ${JENKINS_CONFIG}
sed -i 's/JENKINS_PORT\=\"8080\"/JENKINS_PORT\=\"8180\"/g' ${JENKINS_CONFIG}
sed -i 's/JENKINS_AJP_PORT\=\"8009\"/JENKINS_AJP_PORT\=\"8109\"/g' ${JENKINS_CONFIG}

# Auto Srart
chkconfig jenkins on

# Avalable Shell Login
# sed -i 's/jenkins\:\/bin\/false/jenkins\:\/bin\/bash/'g /etc/passwd
usermod -d /var/lib/jenkins -s /bin/bash jenkins

# download plugin
PLUGIN="https://updates.jenkins-ci.org/latest/bitbucket.hpi
https://updates.jenkins-ci.org/latest/bitbucket-pullrequest-builder.hpi
https://updates.jenkins-ci.org/latest/bitbucket-oauth.hpi
https://updates.jenkins-ci.org/latest/bitbucket-approve.hpi
https://updates.jenkins-ci.org/latest/gradle.hpi
https://updates.jenkins-ci.org/latest/git.hpi
https://updates.jenkins-ci.org/latest/git-client.hpi
https://updates.jenkins-ci.org/latest/distTest.hpi
https://updates.jenkins-ci.org/latest/junit-attachments.hpi
https://updates.jenkins-ci.org/latest/junit.hpi
http://updates.jenkins-ci.org/latest/deploy.hpi
http://updates.jenkins-ci.org/latest/jacoco.hpi
https://updates.jenkins-ci.org/latest/timestamper.hpi"

for i in ${PLUGIN}
do
  wget -v -t 3 -P /var/lib/jenkins/plugins "${i}"
done

# owner chenge
chown jenkins.jenkins -R /var/lib/jenkins/plugins
