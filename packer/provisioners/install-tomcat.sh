#!/usr/bin/env bash

# JavaEE(Tomcat) server install.
TOMCAT="tomcat
tomcat-admin-webapps
tomcat-docs-webapp
tomcat-javadoc
tomcat-jsvc
tomcat-webapps"

for i in ${TOMCAT}
do
  yum -y -R 2 install ${i}
done

# Alternatives for Java version. (openjdk-1.7.0)
alternatives --set java /usr/lib/jvm/jre-1.7.0-openjdk.x86_64/bin/java

# Auto Srart
chkconfig tomcat on

# Configure Tomcat.
USER_CONF="/etc/tomcat/tomcat-users.xml"
USER_ROLE='\n  <!-- user roles -->\n
  <role rolename="admin"/>\n
  <role rolename="admin-gui"/>\n
  <role rolename="admin-script"/>\n
  <role rolename="manager"/>\n
  <role rolename="manager-gui"/>\n
  <role rolename="manager-script"/>\n
  <role rolename="manager-jmx"/>\n
  <role rolename="manager-status"/>\n
\n
  <!-- tomcat users -->\n
  <user name="admin" password="admin" roles="admin,manager,admin-gui,admin-script,manager-gui,manager-script,manager-jmx,manager-status" />'

USERS=$(echo ${USER_ROLE} |sed 's/\//\\\//g')

sed -i "/<\/tomcat-users>/ s/.*/${USERS}\n&/" /etc/tomcat/tomcat-users.xml


