#!/usr/bin/env bash

cat << EOM >> /etc/yum.repos.d/wandisco.repo
[wandisco-git]
name=WANdisco Repository - git centos6
baseurl=http://opensource.wandisco.com/centos/6/git/x86_64/$basearch
enabled=0
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-WANdisco
EOM

rpm --import http://opensource.wandisco.com/RPM-GPG-KEY-WANdisco
yum -y -R 2 install git --enablerepo=wandisco-git
