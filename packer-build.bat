@echo off
set BOXES=.\packer\vagrant-boxes\CentOS-6.6-x86_64-minimal.box
dir %BOXES% >NUL 2>&1
If Errorlevel 0 del /a %BOXES% >NUL 2>&1
packer build -only=virtualbox-iso packer.json
